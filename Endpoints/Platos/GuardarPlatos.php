<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/inventario/routes.php';

require_once CONTROLLER . 'Platos/PlatosController.php';


use Controllers\platos\PlatosController;

/** @var type $data */
$data = json_decode($_POST['data'], true);

$obj = new PlatosController();


$obj->crearPlato($data['precio'], $data['codigo'], $data['tipo_plato'], $data['descripcion']);


