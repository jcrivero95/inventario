<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/inventario/routes.php';

require_once CONTROLLER . 'Inventario/InventarioController.php';


use Controllers\inventario\InventarioController;

/** @var type $data */
$data = json_decode($_POST['data'], true);

$obj = new InventarioController();


$obj->crearProducto($data['codigo'], $data['nombre'], $data['descripcion'], $data['precio'], $data['stock'], $data['categoria'], $data['estado']);


