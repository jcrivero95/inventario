<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/inventario/routes.php';

require_once CONTROLLER . 'Mesas/MesasController.php';


use Controllers\Mesas\MesasController;

/** @var type $data */
$data = json_decode($_POST['data'], true);

$obj = new MesasController();


$obj->crearMesas($data['codigo'], $data['cantidad'], $data['ubicacion'], $data['estado']);


