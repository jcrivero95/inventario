<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/inventario/routes.php';

require_once CONTROLLER . 'Users/UserController.php';


use Controllers\Users\UserController;

/** @var type $data */
$data = json_decode($_POST['data'], true);

$obj = new UserController();


$obj->crearUsuario($data['email'], $data['password'], $data['rol'], $data['nombres'], $data['apellidos'], $data['telefono']);


