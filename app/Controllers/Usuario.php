<?php

namespace App\Controllers;

use App\Models\User_model;

class Usuario extends BaseController
{

    public function __construct()
    {
        $this->usuario_model = new User_model();
    }

    public function index()
    {
        $mensaje = session('mensaje');
        return view('login', ["mensaje" => $mensaje]);
    }

    public function crear_usuario()
    {
        return view('Usuario/CrearUsuario');

    }

    public function listado_usuario()
    {

        $modelo = $this->usuario_model;
        $query = $modelo->getUsuarios();
        $datos['usuarios'] = $query->getResultArray();
        return view('Usuario/ListarUsuarios', $datos);

    }

}
