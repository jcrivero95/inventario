<?php

namespace App\Controllers;

use App\Models\Mesa_model;

class Mesas extends BaseController
{

    public function __construct()
    {
        $this->mesa_model = new Mesa_model();
    }

    public function index()
    {
        $mensaje = session('mensaje');
        return view('login', ["mensaje" => $mensaje]);
    }

    public function crear_mesas()
    {
        return view('Mesas/CrearMesas');

    }

    public function listado_mesas()
    {

        $modelo = $this->mesa_model;
        $query = $modelo->getMesas();
        $datos['mesas'] = $query->getResultArray();
        return view('Mesas/ListarMesas', $datos);

    }

}
