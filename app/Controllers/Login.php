<?php

namespace App\Controllers;

use App\Models\User_model;

class Login extends BaseController
{

    public function login()
    {
        $usuario = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $Usuario = new User_model();

        $datosUsuario = $Usuario->obtenerUsuario(['email' => $usuario]);

        if (count($datosUsuario) > 0 && password_verify($password, $datosUsuario[0]['password'])) {

            $data = [
                "usuario" => $datosUsuario[0]['email'],
                "rol" => $datosUsuario[0]['rol'],
                 "logueado" => $datosUsuario[0]['logueado']
            ];
            $session = session();
            $session->set($data);

            return redirect()->to(base_url('/inicio'))->with('mensaje', '1');

        } else {
            return redirect()->to(base_url('/'))->with('mensaje', 'Email o contraseña incorrecto');
        }
    }

    public function salir()
    {
        $session = session();
        $session->destroy();
        return redirect()->to(base_url('/'));
    }

}
