<?php

namespace App\Controllers;

use App\Models\Inventario_model;

class Inventario extends BaseController
{
    public function __construct()
    {
        $this->inventario_model = new Inventario_model();
    }

    public function index()
    {
        $mensaje = session('mensaje');
        return view('login', ["mensaje" => $mensaje]);
    }

    public function crear_categoria()
    {

        return view('Inventario/CrearCategoria');

    }

    public function crear_producto()
    {
        $modelo = $this->inventario_model;
        $query = $modelo->getCategorias();
        $datos['categorias'] = $query->getResultArray();
        return view('Inventario/CrearProducto', $datos);

    }

    public function listado_producto()
    {
        $modelo = $this->inventario_model;
        $query = $modelo->getProductos();
        $datos['productos'] = $query->getResultArray();
        return view('Inventario/ListarProducto', $datos);

    }


    public function crear_inventario()
    {
        $modelo = $this->inventario_model;
        $query = $modelo->getProductos();
        $datos['productos'] = $query->getResultArray();
        return view('Inventario/Inventario', $datos);

    }




}
