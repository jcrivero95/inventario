<?php

namespace App\Controllers;

use App\Models\User_model;

class Home extends BaseController
{
    public function index()
    {
        $mensaje = session('mensaje');
        return view('login', ["mensaje" => $mensaje]);
    }

    public function inicio()
    {
        return view('layout/content');

    }

}
