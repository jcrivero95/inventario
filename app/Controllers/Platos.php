<?php

namespace App\Controllers;

use App\Models\Platos_model;

class Platos extends BaseController
{
    public function __construct()
    {
        $this->platos_model = new Platos_model();
    }

    public function index()
    {
        $mensaje = session('mensaje');
        return view('login', ["mensaje" => $mensaje]);
    }

    public function crear_plato()
    {
        $modelo = $this->platos_model;
        $query = $modelo->getTipoPlato();
        $datos['tipos'] = $query->getResultArray();
        return view('Platos/CrearPlatos', $datos);

    }


    public function tipo_plato()
    {



        echo view('Platos/TiposPlatos');

    }

}
