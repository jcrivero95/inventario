<?php

namespace APP\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class SessionUsuarioLogueado implements FilterInterface
{

    public function before(RequestInterface $request, $arguments = null)
    {
        // TODO: Implement before() method.
        if (session('logueado') != 1) {
            return redirect()->to(base_url('/'));
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // TODO: Implement after() method.
    }
}