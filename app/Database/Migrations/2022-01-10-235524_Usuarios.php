<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Usuarios extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_usuario'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'nombres'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'apellidos'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'telefono'       => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
            ],
            'email'       => [
                'type'       => 'VARCHAR',
                'constraint' => '250',
            ],
            'password'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'rol'       => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
        ]);
        $this->forge->addKey('id_usuario', true);
        $this->forge->createTable('usuarios');
    }

    public function down()
    {
        $this->forge->dropTable('usuarios');
    }
}
