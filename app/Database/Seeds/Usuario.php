<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Usuario extends Seeder
{
    public function run()
    {
        $email = "jcrivero98@gmail.com";
        $password = password_hash("juan017*", PASSWORD_DEFAULT);
        $rol = 0;
        $data = [
            'email' => $email,
            'password'    => $password,
            'rol' => $rol
        ];


        $this->db->table('usuarios')->insert($data);
    }
}
