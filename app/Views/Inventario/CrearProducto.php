<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </div>
                        <div>Productos
                            <div class="page-title-subheading">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Formulario crear productos</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Codigo</label>
                                    <input type="text" class="form-control" id="codigo" required>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Precio</label>
                                    <input type="number" class="form-control" id="precio" required>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Stock</label>
                                    <input type="number" class="form-control" id="stock" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Categoria</label>
                                    <select class="form-control" id="categoria">
                                        <option value="">Seleccione...</option>
                                        <?php if (!empty($categorias) ): ?>
                                            <?php foreach ($categorias as $c): ?>
                                                <option value="<?= esc($c["id_categoria"]) ?>"><?= esc($c["nombre"]) ?></option>
                                            <?php endforeach ?>

                                        <?php else : ?>
                                            <option value="">No hay categorías</option>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Estado</label>
                                    <select class="form-control" id="estado">
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationCustomUsername">Descripción del producto</label>
                                <div class="input-group">
                                    <textarea name="text" id="descripcion" class="form-control"></textarea>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="button" onclick="crearProducto()">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


<?= $this->include('layout/footer') ?>