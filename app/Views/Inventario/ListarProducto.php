<?= $this->include('layout/header') ?>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    </div>
                    <div>Listados de Productos
                        <div class="page-title-subheading">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <h5 class="card-title">Listado de Productos</h5>
                        </div>

                        <div class="col-md-6" align="right">
                            <a class="btn btn-primary" href="<?php echo base_url() ?>/inventario/crear_producto">Crear Producto</a>
                        </div>

                    </div>

                    <br>
                    <table class="table table-striped" id="example" >
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Estado</th>
                            <th scope="col" align="right"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($productos)): ?>
                            <?php foreach ($productos as $row): ?>
                                <tr>
                                    <th scope="row"><?= esc($row["id_producto"]) ?></th>
                                    <th scope="row"><?= esc($row["codigo"]) ?></th>
                                    <th scope="row"><?= esc($row["codigo"]) ?></th>
                                    <th scope="row"><?= esc($row["codigo"]) ?></th>
                                    <th scope="row"><?= esc($row["codigo"]) ?></th>
                                    <?php if ($row["estado"] == 1): ?>
                                        <td> <div class="badge badge-success">Activo</div></td>

                                    <?php else : ?>
                                        <td> <div class="badge badge-danger">Agotado</div></td>
                                    <?php endif ?>
                                    <td align="right"><button class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button>
                                        <button class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                             Eliminar</button></td>


                                </tr>
                            <?php endforeach ?>

                        <?php else : ?>
                            <tr>
                                <td colspan="6" align="center">No hay registro</td>
                            </tr>
                        <?php endif ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


    <?= $this->include('layout/footer') ?>

