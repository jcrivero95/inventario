<?= $this->include('layout/header'); ?>
    <!--require_once QUERIES . 'db.php';
    $tipos = get_tiposPlatos();-->

    <div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-random" aria-hidden="true"></i>
                    </div>
                    <div>Crear Categoría
                        <div class="page-title-subheading">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Formulario crear categoría</h5>
                    <form class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Nombre</label>
                                <input type="text" class="form-control" id="nombre" required>
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationCustomUsername">Descripción</label>
                                <div class="input-group">
                                    <textarea name="text" id="descripcion" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>


                        <button class="btn btn-primary" type="button" onclick="crearCategoria()">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>


<?= $this->include('layout/footer') ?>