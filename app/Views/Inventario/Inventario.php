<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph"> </i>
                        </div>
                        <div>Inventario
                            <div class="page-title-subheading">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Inventario</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Codigo del producto</label>
                                    <select class="form-control" id="codigo_producto" onchange="cargarProducto()">
                                        <option value="">Seleccione...</option>
                                        <?php if (!empty($productos) ): ?>
                                            <?php foreach ($productos as $c): ?>
                                                <option value="<?= esc($c["id_producto"]) ?>"><?= esc($c["codigo"]) ?></option>
                                            <?php endforeach ?>

                                        <?php else : ?>
                                            <option value="">No hay registro</option>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Producto</label>
                                    <input type="text" class="form-control" id="producto" name="producto" required>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Categoria</label>
                                    <input type="text" class="form-control" id="categoria" name="categoria" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Tipo de Movimiento</label>
                                    <select class="form-control" id="categoria">
                                        <option value="">Seleccione...</option>
                                        <option value="Entrada">Entrada</option>
                                        <option value="Salida">Salida</option>
                                    </select>
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Unidades</label>
                                    <input type="number" class="form-control" id="unidades" required>
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Existencia del producto</label>
                                    <input type="number" class="form-control" id="stock" required>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <label for="validationCustom01">Total</label>
                                    <input type="number" id="total" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Periodo</label>

                                    <input type="date" class="form-control" id="periodo">

                                </div>
                            </div>

                            <button class="btn btn-primary" type="button" onclick="crearProducto()">Guardar Inventario</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


<?= $this->include('layout/footer') ?>