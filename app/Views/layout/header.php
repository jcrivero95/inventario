<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Inventario</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>
    <meta name="description" content="Wide selection of modal dialogs styles and animations available.">
    <meta name="msapplication-tap-highlight" content="no">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>/main.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/css/loader.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <div class="app-header header-shadow">
        <div class="app-header__logo">
            <!--<div class="logo-src"></div>-->
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
                <span>
                    <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
        </div>
        <div class="app-header__content">
            <div class="app-header-right">
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <span><i class="fa fa-user"></i> <?php echo session('usuario') ?> <i
                                                    class="fa fa-sort-desc" aria-hidden="true"></i></span>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                         class="dropdown-menu dropdown-menu-right">
                                        <button type="button" tabindex="0" class="dropdown-item">Perfil</button>
                                        <button type="button" tabindex="0" class="dropdown-item">Configuracion</button>
                                        <button type="button" tabindex="0" class="dropdown-item">Cambiar contraseña
                                        </button>
                                        <div tabindex="-1" class="dropdown-divider"></div>
                                        <a href="<?php echo base_url() ?>/login/salir" tabindex="0"
                                           class="dropdown-item">Salir</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui-theme-settings">
        <div class="theme-settings__inner">
            <div class="scrollbar-container">
                <div class="theme-settings__options-wrapper">
                    <h3 class="themeoptions-heading">Layout Options
                    </h3>
                    <div class="p-3">
                        <ul class="list-group">

                            <li class="list-group-item">
                                <div class="widget-content p-0">

                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-main">
        <div class="app-sidebar sidebar-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                        <span>
                            <button type="button"
                                    class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
            </div>
            <div class="scrollbar-sidebar">
                <div class="app-sidebar__inner">
                    <ul class="vertical-nav-menu">
                        <li class="app-sidebar__heading">Inicio</li>

                        <li class="app-sidebar__heading"></li>

                         <?php if(session('rol') == 0){?>
                        <li>
                            <a href="#">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-link"></i>
                                    Crear
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul class="mm-show">
                                    <li>
                                        <a href="<?php echo base_url() ?>/usuario/listado_usuario">
                                            <i class="fa fa-users"></i>
                                            Usuarios
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>/mesas/listado_mesas">
                                            <i class="fa fa-coffee" aria-hidden="true"></i>
                                            </i>Mesas
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <i class="fa fa-life-ring" aria-hidden="true"></i>
                                            </i>Platos
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url() ?>/platos/tipo_plato">
                                                    Tipos de Platos
                                                </a>
                                            </li>
                                            <li>
                                            <a href="<?php echo base_url() ?>/platos/crear_plato">
                                                Platos
                                            </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url() ?>/usuario/crear_usuario">
                                            <i class="metismenu-icon">
                                            </i>Tareas
                                        </a>
                                    </li>

                                </ul>
                        </li>

                        <?php } ?>
                        <li>
                            <a href="#">
                                <i class="metismenu-icon pe-7s-link"></i>
                                Inventario
                                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                            </a>
                            <ul class="mm-show">
                                <li>
                                    <a href="<?php echo base_url() ?>/inventario/crear_categoria">
                                        <i class="fa fa-random" aria-hidden="true"></i>
                                        Categorias
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url() ?>/inventario/crear_producto">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Producto
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url() ?>/inventario/listado_producto">
                                        <i class="fa fa-align-justify" aria-hidden="true"></i>
                                        Listados de Productos
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url() ?>/inventario/crear_inventario">
                                        <i class="fa fa-balance-scale" aria-hidden="true"></i>
                                        Inventario
                                    </a>
                                </li>


                            </ul>
                </div>
            </div>
        </div>

