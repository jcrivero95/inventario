<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="fa fa-users">
                            </i>
                        </div>
                        <div>Crear Usuarios
                            <div class="page-title-subheading">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Formulario crear usuario</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Nombres</label>
                                    <input type="text" class="form-control" id="nombres" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos"  required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Telefono</label>
                                    <input type="number" class="form-control" id="telefono"  required>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Email</label>
                                    <input type="email" class="form-control" id="email" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Contraseña</label>
                                    <input type="password" class="form-control" id="password"  required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Rol</label>
                                    <div class="input-group">
                                        <select class="form-control" id="rol">
                                            <option value="">Seleccione...</option>
                                            <option value="0">Administrador</option>
                                            <option value="1">Usuario</option>
                                        </select>

                                    </div>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="button" onclick="saveUser()">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


<?= $this->include('layout/footer') ?>