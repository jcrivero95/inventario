<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-users">
                        </i>
                    </div>
                    <div>Usuarios
                        <div class="page-title-subheading">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <h5 class="card-title">Listado de Usuario</h5>
                        </div>

                        <div class="col-md-6" align="right">
                            <a class="btn btn-primary" href="<?php echo base_url()?>/usuario/crear_usuario">Crear Usuario</a>
                        </div>

                    </div>

                    <br>
                    <table class="table table-striped" id="example">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Email</th>
                            <th scope="col">Teléfono</th>
                            <th scope="col">Rol</th>
                            <th scope="col">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($usuarios)): ?>
                            <?php foreach ($usuarios as $u): ?>
                                <tr>
                                    <th scope="row"><?= esc($u["id_usuario"]) ?></th>
                                    <td><?= esc($u["nombres"]) ?></td>
                                    <td><?= esc($u["apellidos"]) ?></td>
                                    <td><?= esc($u["email"]) ?></td>
                                    <td><?= esc($u["telefono"]) ?></td>
                                    <td><?= esc($u["nombre"]) ?></td>
                                    <td></td>
                                </tr>
                            <?php endforeach ?>

                        <?php else : ?>
                            <tr>
                                <td colspan="6" align="center">No hay registro</td>
                            </tr>
                        <?php endif ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


<?= $this->include('layout/footer') ?>