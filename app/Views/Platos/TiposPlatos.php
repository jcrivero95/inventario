<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="fa fa-life-ring" aria-hidden="true"></i>
                        </div>
                        <div>Tipos de platos
                            <div class="page-title-subheading">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Formulario tipos de plato</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Codigo</label>
                                    <input type="text" class="form-control" id="codigo" required>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" required>
                                </div>

                            </div>


                            <button class="btn btn-primary" type="button" onclick="crearTipoPlato()">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


<?= $this->include('layout/footer') ?>