<?= $this->include('layout/header'); ?>
    <!--require_once QUERIES . 'db.php';
    $tipos = get_tiposPlatos();-->

    <div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-life-ring" aria-hidden="true"></i>
                    </div>
                    <div>Crear Platos
                        <div class="page-title-subheading">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Formulario crear plato</h5>
                    <form class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Código</label>
                                <input type="text" class="form-control" id="codigo" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Precio</label>
                                <input type="number" class="form-control" id="precio" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Tipo de plato</label>
                                <div class="input-group">
                                    <select class="form-control" id="tipo_plato">
                                        <option value="">Seleccione...</option>
                                        <?php if (!empty($tipos) ): ?>
                                            <?php foreach ($tipos as $t): ?>
                                                <option value="<?= esc($t["id_tipo_plato"]) ?>"><?= esc($t["nombre"]) ?></option>
                                            <?php endforeach ?>

                                        <?php else : ?>
                                            <option value="">No hay tipos de plato registrados</option>
                                        <?php endif ?>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="validationCustomUsername">Descripción del plato</label>
                                <div class="input-group">
                                    <textarea name="text" id="descripcion" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>


                        <button class="btn btn-primary" type="button" onclick="crearPlato()">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>


<?= $this->include('layout/footer') ?>