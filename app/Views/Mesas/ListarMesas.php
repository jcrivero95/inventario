<?= $this->include('layout/header') ?>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fa fa-users">
                        </i>
                    </div>
                    <div>Mesas
                        <div class="page-title-subheading">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">
                            <h5 class="card-title">Listado de Mesas</h5>
                        </div>

                        <div class="col-md-6" align="right">
                            <a class="btn btn-primary" href="<?php echo base_url() ?>/mesas/crear_mesas">Crear Mesas</a>
                        </div>

                    </div>

                    <br>
                    <table class="table table-striped" id="example">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Ubicacion</th>
                            <th scope="col">Estado</th>
                            <th scope="col" align="right"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($mesas)): ?>
                            <?php foreach ($mesas as $row): ?>
                                <tr>
                                    <th scope="row"><?= esc($row["id_mesa"]) ?></th>
                                    <th scope="row"><?= esc($row["codigo"]) ?></th>
                                    <td><?= esc($row["ubicacion"]) ?></td>
                                    <?php if ($row["estado"] == 1): ?>
                                        <td>habilitada</td>

                                    <?php else : ?>
                                        <td>Inhabilitada</td>
                                    <?php endif ?>
                                    <td align="right"><button class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button>
                                        <button class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                             Eliminar</button></td>


                                </tr>
                            <?php endforeach ?>

                        <?php else : ?>
                            <tr>
                                <td colspan="6" align="center">No hay registro</td>
                            </tr>
                        <?php endif ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


    <?= $this->include('layout/footer') ?>

