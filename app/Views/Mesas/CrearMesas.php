<?= $this->include('layout/header') ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="fa fa-coffee" aria-hidden="true"></i>
                        </div>
                        <div>Crear Mesas
                            <div class="page-title-subheading">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Formulario crear mesa</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Código</label>
                                    <input type="text" class="form-control" id="codigo" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Cantidad de comensal</label>
                                    <input type="number" class="form-control" id="cantidad"  required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Ubicación</label>
                                    <input type="text" class="form-control" id="ubicacion"  required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Estado</label>
                                    <div class="input-group">
                                        <select class="form-control" id="estado">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Habilitada</option>
                                            <option value="0">Inhabilitada</option>
                                        </select>

                                    </div>
                                </div>
                            </div>


                            <button class="btn btn-primary" type="button" onclick="crearMesa()">Guardar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


<?= $this->include('layout/footer') ?>