<div class="inbox-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="inbox-left-sd">
                    <div class="compose-ml">
                        <a class="btn" href="#">Menu</a>
                    </div>
                    <div class="inbox-status">
                        <ul class="inbox-st-nav inbox-ft">
                            <li><a href="#"><i class="fa fa-user"></i> Crear Usuario</a></li>

                        </ul>
                    </div>
                    <hr>
                    <div class="inbox-status">
                        <ul class="inbox-st-nav inbox-nav-mg">
                            <li><a href="#"><i class="notika-icon notika-success"></i> Crear plato</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                <div class="view-mail-list sm-res-mg-t-30">
                    <div class="view-mail-hd">
                        <div class="view-mail-hrd">
                            <h2>Crear usuario</h2>
                        </div>
                    </div>
                    <div class="cmp-int mg-t-20">
                        <div class="row">
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                <div class="cmp-int-lb cmp-int-lb1 text-right">
                                    <span>Username </span>
                                </div>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st cmp-int-in cmp-email-over">
                                        <input type="text" class="form-control" placeholder="" id="username"
                                               name="username"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                <div class="cmp-int-lb cmp-int-lb1 text-right">
                                    <span>Password: </span>
                                </div>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st cmp-int-in cmp-email-over">
                                        <input type="password" class="form-control" placeholder="" id="password" name="password"/>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                <div class="cmp-int-lb cmp-int-lb1 text-right">
                                    <span>Rol: </span>
                                </div>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                <div class="form-group">
                                    <div class="nk-int-st cmp-int-in cmp-email-over">
                                        <div class="bootstrap-select fm-cmp-mg">
                                            <select class="selectpicker" data-live-search="true" id="rol" name="rol">
                                                <option value="0">Admin</option>
                                                <option value="1">Users</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-link" type="button" onclick="saveUser()"><i class="fa fa-save" aria-hidden="true"></i></button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
