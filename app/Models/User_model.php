<?php


namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;


class User_model extends Model
{
    public function obtenerUsuario($data){
        $Usuario = $this->db->table('usuarios');
        $Usuario->where($data);
        return $Usuario->get()->getResultArray();

    }

    public function getUsuarios()
    {
        $data = $this->db->query("SELECT  * FROM usuarios as u inner join roles as r on r.codigo = u.rol");
        return $data;
    }

}