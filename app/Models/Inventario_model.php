<?php


namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;


class Inventario_model extends Model
{



    public function getCategorias()
    {
        $data = $this->db->query("SELECT * FROM categorias where estado = 1");
        return $data;
    }

    public function getProductos()
    {
        $data = $this->db->query("SELECT * FROM producto as p
                                 inner join categorias as c on c.id_categoria =  p.categoria_id");
        return $data;
    }




}