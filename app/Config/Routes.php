<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/inicio', 'Home::inicio');
$routes->post('/login', 'Login::login');
$routes->get('/salir', 'Login::salir');
$routes->get('/crear_usuario', 'Usuario::crear_usuario');
$routes->get('/crear_mesa', 'Mesa::crear_mesa');
$routes->get('/crear_plato', 'Platos::crear_plato');
$routes->get('/tipo_plato', 'Platos::tipo_plato');
$routes->get('/listado_usuario', 'Usuario::listado_usuario');
$routes->get('/crear_categoria', 'Inventario::crear_categoria');
$routes->get('/crear_producto', 'Inventario::crear_producto');
$routes->get('/listado_producto', 'Inventario::listado_producto');
$routes->get('/listado_mesas', 'Mesa::listado_mesas');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
