<?php


namespace Controllers\Users;

require_once REPOSITORY . 'Users/UserRepository.php';
require_once PROJECT . "Helpers/Response.php";


use Helpers\Response;
use Repository\Users\User\UserRepository;


class UserController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new UserRepository();
    }

    public function crearUsuario($email, $password, $rol, $nombres, $apellidos, $telefono)
    {
        return $this->repo->guardarUsuario($email, password_hash($password, PASSWORD_DEFAULT), $rol, $nombres, $apellidos, $telefono);

    }


}
