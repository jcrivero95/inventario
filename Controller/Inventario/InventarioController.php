<?php


namespace Controllers\inventario;

require_once REPOSITORY . 'Inventario/InventarioRepository.php';
require_once PROJECT . "Helpers/Response.php";


use Helpers\Response;
use Repository\Inventarios\Inventario\InventarioRepository;


class InventarioController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new InventarioRepository();
    }

    public function crearCategoria($nombre, $descripcion, $estado)
    {
        return $this->repo->guardarCategoria($nombre, $descripcion, $estado);

    }

    public function crearProducto($codigo, $nombre, $descripcion, $precio, $stock, $categoria, $estado)
    {
        return $this->repo->guardarProducto($codigo, $nombre, $descripcion, $precio, $stock, $categoria, $estado);

    }

    public function consultarProducto($codigo)
    {
        return $this->repo->consultarProductos($codigo);

    }


}
