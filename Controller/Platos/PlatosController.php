<?php


namespace Controllers\platos;

require_once REPOSITORY . 'Platos/PlatosRepository.php';
require_once PROJECT . "Helpers/Response.php";


use Helpers\Response;
use Repository\Platos\Plato\PlatosRepository;


class PlatosController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new PlatosRepository();
    }

    public function crearPlato($precio, $codigo_plato, $tipo, $descripcion)
    {
        return $this->repo->guardarPlato($precio, $codigo_plato, $tipo, $descripcion);

    }

    public function crearTipoPlato($nombre, $codigo)
    {
        return $this->repo->guardarTipoPlato($nombre, $codigo);

    }


}
