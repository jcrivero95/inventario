<?php


namespace Controllers\Mesas;

require_once REPOSITORY . 'Mesas/MesasRepository.php';
require_once PROJECT . "Helpers/Response.php";


use Helpers\Response;
use Repository\Mesas\Mesa\MesasRepository;


class MesasController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new MesasRepository();
    }

    public function crearMesas($codigo, $cantidad, $ubicacion, $estado)
    {
        return $this->repo->guardarMesa($codigo, $cantidad, $ubicacion, $estado);

    }


}
