<?php
define('ROOT', dirname(__FILE__) . '/');
define('PROJECT', ROOT);
define('INFRASTRUCTURE', PROJECT . 'Infrastructure/');
define('CONTROLLER', PROJECT . 'Controller/');
define('REPOSITORY', PROJECT . 'Repository/');
define('VIEW', PROJECT . 'Views/');
define('QUERIES', PROJECT . 'queries/');
define('REQUIERE', PROJECT . 'requiere/');
