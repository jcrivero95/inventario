<?php


namespace Repository\Inventarios\Inventario;

require_once INFRASTRUCTURE . 'BaseRepository.php';
require_once PROJECT . 'Helpers/Response.php';

use BaseRepository;
use Helpers\Response;


class InventarioRepository extends BaseRepository
{
    public function guardarCategoria($nombre, $descripcion, $estado)
    {


        $sql = "insert into categorias (nombre,descripcion,estado) values (?,?,?)";

        $this->execute($sql, 'ssi', [$nombre, $descripcion, $estado]);

        return Response::response('Categoria guardada correctamente', null, true);
    }

    public function guardarProducto($codigo, $nombre, $descripcion, $precio, $stock, $categoria, $estado)
    {
        $sql = "insert into producto (codigo, nombre, descripcion, precio, stock, categoria_id, estado) values (?,?,?,?,?,?,?)";

        $this->execute($sql, 'sssiiii', [$codigo, $nombre, $descripcion, $precio, $stock, $categoria, $estado]);

        return Response::response('Producto guardado correctamente', null, true);
    }

    public function consultarProductos($codigo){

        $sql = "select p.nombre as producto, c.nombre as categoria from producto as p
                inner join categorias as c on c.id_categoria = p.categoria_id
                where id_producto = ?";

       $resul = $this->query($sql, 'i', [$codigo]);

        return Response::json($resul);
    }



}
