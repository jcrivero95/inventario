<?php


namespace Repository\Mesas\Mesa;

require_once INFRASTRUCTURE . 'BaseRepository.php';
require_once PROJECT . 'Helpers/Response.php';

use BaseRepository;
use Helpers\Response;


class MesasRepository extends BaseRepository
{
    public function guardarMesa($codigo, $cantidad, $ubicacion, $estado)
    {
        $sql = "insert into mesas (codigo,cantidad_comesales,ubicacion,estado) values (?,?,?,?)";

        $this->execute($sql, 'issi', [$codigo, $cantidad, $ubicacion, $estado]);

        return Response::response('Mesa guardada correctamente', null, true);
    }



}
