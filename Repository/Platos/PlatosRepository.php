<?php


namespace Repository\Platos\Plato;

require_once INFRASTRUCTURE . 'BaseRepository.php';
require_once PROJECT . 'Helpers/Response.php';

use BaseRepository;
use Helpers\Response;


class PlatosRepository extends BaseRepository
{
    public function guardarPlato($precio, $codigo_plato, $tipo, $descripcion)
    {
        $sql = "insert into platos (precio,cod_plato,tipo_plato,descripcion) values (?,?,?,?)";

        $this->execute($sql, 'isis', [$precio, $codigo_plato, $tipo, $descripcion]);

        return Response::response('Plato guardado correctamente', null, true);
    }

    public function guardarTipoPlato($nombre, $codigo)
    {
        $sql = "insert into tipo_plato (nombre, codigo) values (?,?)";

        $this->execute($sql, 'ss', [$nombre, $codigo]);

        return Response::response('Tipo de Plato guardado correctamente', null, true);
    }



}
