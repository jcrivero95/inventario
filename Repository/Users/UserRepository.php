<?php


namespace Repository\Users\User;

require_once INFRASTRUCTURE . 'BaseRepository.php';
require_once PROJECT . 'Helpers/Response.php';

use BaseRepository;
use Helpers\Response;


class UserRepository extends BaseRepository
{
    public function guardarUsuario($username, $password, $rol, $nombres, $apellidos, $telefono)
    {
        $sql = "insert into usuarios (email,password,rol,nombres,apellidos,telefono) values (?,?,?,?,?,?)";

        $this->execute($sql, 'ssisss', [$username, $password, $rol, $nombres, $apellidos, $telefono]);

        return Response::response('Usuario guardado correctamenta', null, true);
    }



}
