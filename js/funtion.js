function message(response = {}) {
    const {success = false, message = 'Algo salió mal'} = response;
    if (success) {
        alertify.success(message);
    } else {
        alertify.error(message);
    }

}

function post(url, data) {
    $('#myModal').show();
    $.ajax({
        url,
        data: 'data=' + encodeURIComponent(JSON.stringify(data)),
        type: 'post',
        dataType: "json",
        crossDomain: false,
        success: function (response) {
            message(response);

        },
        error: function (r) {
            message('Algo salió mal');
        },
        complete: () => {
            $('#myModal').hide();
        }
    });
}

function saveUser() {
    let password = document.getElementById("password").value;
    let email = document.getElementById("email").value;
    let rol = document.getElementById("rol").value;
    let nombres = document.getElementById("nombres").value;
    let apellidos = document.getElementById("apellidos").value;
    let telefono = document.getElementById("telefono").value;

    const url = '../Endpoints/Users/saveUsers.php'

    if (password == "" || email == "" || rol == "" || nombres == "" || apellidos == "" || telefono == "") {
        alert('Diligencie todos los campos');
        return null;
    }

    var data = {
        email,
        password,
        rol,
        nombres,
        apellidos,
        telefono

    }


    post(url, data);


}

function crearMesa(){
    let codigo = document.getElementById("codigo").value;
    let cantidad = document.getElementById("cantidad").value;
    let ubicacion = document.getElementById("ubicacion").value;
    let estado = document.getElementById("estado").value;
    if (codigo == "" || ubicacion == "" || estado == "") {
        alertify.error('Diligencie todos los campos de Código, Ubicación y Estado');
        return null;
    }

    const url = '../Endpoints/Mesas/GuardarMesas.php'

    var data = {
        codigo,
        cantidad,
        ubicacion,
        estado
    }

    post(url,  data);

}


function crearTipoPlato() {


    let nombre = document.getElementById("nombre").value;
    let codigo = document.getElementById("codigo").value;

        if (nombre == "" || codigo == "") {
            alertify.error('Complete todos los campos');
            return null;
        }

        const url = '../Endpoints/Platos/GuardarTipoPlatos.php'

        var data = {
            nombre,
            codigo

        }

        post(url, data);


}

function crearPlato(){
    let precio = document.getElementById("precio").value;
    let codigo = document.getElementById("codigo").value;
    let tipo_plato = document.getElementById("tipo_plato").value;
    let descripcion = document.getElementById("descripcion").value;

    if (precio == "" || codigo == "" || tipo_plato == "" ) {
        alertify.error('Complete los campos requeridos');
        return null;
    }

    const url = '../Endpoints/Platos/GuardarPlatos.php'

    var data = {
        precio,
        codigo,
        tipo_plato,
        descripcion

    }

    post(url, data);
}

function crearCategoria(){
    let nombre = document.getElementById("nombre").value;
    let descripcion = document.getElementById("descripcion").value;
    let estado = 1

    if (nombre == "" ) {
        alertify.error('Es requerido agregar un nombre a la categoría');
        return null;
    }

    const url = '../Endpoints/Inventario/GuardarCategoria.php'

    var data = {
        nombre,
        descripcion,
        estado

    }

    post(url, data);
}


function crearProducto(){
    let nombre = document.getElementById("nombre").value;
    let descripcion = document.getElementById("descripcion").value;
    let stock = document.getElementById("stock").value;
    let precio = document.getElementById("precio").value;
    let estado = document.getElementById("estado").value;
    let categoria = document.getElementById("categoria").value;
    let codigo = document.getElementById("codigo").value;


    if (nombre == "" || codigo == "" || precio == "" || estado == "" || categoria == "" || stock == "") {
        alertify.error('Complete los campos requeridos');
        return null;
    }

    const url = '../Endpoints/Inventario/GuardarProducto.php'

    var data = {
        codigo,
        nombre,
        descripcion,
        precio,
        stock,
        categoria,
        estado

    }

    post(url, data);
}

function cargarProducto() {
    let codigo = document.getElementById('codigo_producto').value;

    const url = '../Endpoints/Inventario/consultarProducto.php'
    var data = {
        codigo
    }

    $.ajax({
        url,
        method: "POST",
        data: 'data=' + encodeURIComponent(JSON.stringify(data)),
        success: function (respuesta) {
            console.log(respuesta)
            var json = jQuery.parseJSON(respuesta);
            console.log(json);
            console.log(json[0].producto);

            $('#producto').val(json[0].producto);
            $('#categoria').val(json[0].categoria);

            console.log('ok')
        },
        error:function (respuesta){
            alert("algo salio mal")
        }
    })
}









